package com.finn.sys.base.service;

import com.finn.core.utils.PageResult;
import com.finn.sys.base.query.SysAttachmentQuery;
import com.finn.sys.base.vo.SysAttachmentVO;

import java.util.List;

/**
 * 附件管理
 *
 * @author 王小费 whx5710@qq.com
 *
 */
public interface SysAttachmentService {

    PageResult<SysAttachmentVO> page(SysAttachmentQuery query);

    void save(SysAttachmentVO vo);

    void update(SysAttachmentVO vo);

    void delete(List<Long> idList);
}